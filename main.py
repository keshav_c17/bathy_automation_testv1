#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : Bathymetry Automation    Filename: main.py
# @Version : N/A
# @Date    : 01/12/2021
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" MODULE INFORMATION """
# '''===================================================================================================================
import os

from Utilities.bathy_utils import get_meshgrid, format_zgrid_js
import argparse
import sys


def generate_js_file():

    template_file_2D = os.getcwd()+"/Resources/template_2D_plotly.js"

    with open(template_file_2D, "r") as tmp_file_obj:
        js_file = tmp_file_obj.readlines()
        js_file[1] = js_file[1].split(",")[0] + f"{format_zgrid_js()},\n"               # Adding z_grid to js file
        js_file[2] = js_file[2].split(",")[0] + f"{get_meshgrid(xi=True).tolist()},\n"  # Adding x_array to js file
        js_file[3] = js_file[3].split(",")[0] + f"{get_meshgrid(yi=True).tolist()},\n"  # Adding y_array to js file

    # Creating the js file for 2D graph
    with open(sys.argv[3]+"_2D.js", "w") as output_file_obj:
        data_line = js_file[0].split(" ")
        variable_name = data_line[1]
        data_line[1] = data_line[1].replace(variable_name, sys.argv[2]+"_2D")
        js_file[0] = " ".join(data_line)

        output_file_obj.writelines(js_file)

    # Creating the js file for 3D graph
    with open(sys.argv[3]+"_3D.js", "w") as output_file_obj:
        data_line = js_file[0].split(" ")
        variable_name = data_line[1]
        data_line[1] = data_line[1].replace(variable_name, sys.argv[2]+"_3D")
        js_file[0] = " ".join(data_line)

        type_line = js_file[4].split()
        type_line[1] = type_line[1].replace('contour', 'surface') + "\n"
        js_file[4] = "\t  "+" ".join(type_line)
        output_file_obj.writelines(js_file)
    tmp_file_obj.close()
    output_file_obj.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("path", help="Enter path xyz file.")
    parser.add_argument("variable_name", help="Enter variable name")
    parser.add_argument("output_file_name", help="Enter the name of the output file.")
    args = parser.parse_args()

    generate_js_file()

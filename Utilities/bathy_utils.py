#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : Bathymetry Automation    Filename: bathy_utils.py
# @Version : N/A
# @Date    : 02/12/2021
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" MODULE INFORMATION """
# '''===================================================================================================================
import os
import sys
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import plotly
from scipy.interpolate import griddata


xyz_file = sys.argv[1]
df = pd.read_csv(xyz_file, delimiter=" ", names=["x", "y", "z"])

nines = ""
for num in df.z:
    if str(int(num))[:3] == "-99":
        nines = num
        break

df = df[df.z != int(nines)]


def get_array(x=False, y=False, z=False, xy=False):
    if x:
        return df.x
    elif y:
        return df.y
    elif xy:
        return df.x, df.y
    elif z:
        return df.z
    return df


def get_meshgrid(xi=False, yi=False):
    if xi:
        X = get_array(x=True)
        xi = np.linspace(X.min(), X.max(), num=229)
        return xi
    elif yi:
        Y = get_array(y=True)
        yi = np.linspace(Y.min(), Y.max(), num=229)
        return yi

    else:
        xi = get_meshgrid(xi=True)
        yi = get_meshgrid(yi=True)
        x_grid, y_grid = np.meshgrid(xi, yi)
        return x_grid, y_grid


def create_z_grid():
    return griddata(get_array(xy=True), get_array(z=True), get_meshgrid(), method='cubic')


def format_zgrid_js():
    fig = go.Figure(go.Surface(x=get_meshgrid(xi=True), y=get_meshgrid(yi=True), z=create_z_grid()))
    plotly.io.write_html(fig, "graph.html", full_html=False, include_plotlyjs="cdn", include_mathjax=False)

    with open("graph.html", "r") as file:
        html_file = file.read()
        z = html_file[html_file.find('"z"') + 4:html_file.find(',"type"')]
        file.close()
        os.remove("graph.html")
    return z

